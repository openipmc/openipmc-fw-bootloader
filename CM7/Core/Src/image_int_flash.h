/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/*
 *  Author: André Muller Cascadan, Carlos R. Dell'Aquila
 */

#ifndef IMAGE_INT_FLASH_H
#define IMAGE_INT_FLASH_H

#include <stdbool.h>

// Flash characteristics
#define INT_FLASH_ORIGIN       0x8000000  // First address of Flash in absolute address
#define INT_FLASH_SECTOR_SIZE    0x20000  // Size of erasable sector
#define INT_FLASH_PROG_SIZE           32  // Flash is programmed in groups of 256 bits (32 bytes)


void image_int_flash_open( int sector_number );
int  image_int_flash_write( uint8_t* data, int len );
int  image_int_flash_close( void );
bool image_int_flash_CRC_is_valid( int sector_number, uint32_t expected_crc32 );
bool image_int_flash_CRC_is_valid_use_tail( int sector_number );

#endif /* IMAGE_INT_FLASH_H */
