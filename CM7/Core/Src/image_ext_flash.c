
/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/*
 * This file implements dedicated functions to read firmware images
 * from the external flash memory (Winbond W25N01GV), and others related tools.
 */

#include "main.h"

#include "w25n01gv.h"
#include "fw_metadata.h"

// Flash characteristics
#define BLOCK_SIZE   131072   // Size of erasable block: 128KB or 64 pages = 131072 bytes
#define PAGE_SIZE      2048   // Flash is programmed in pages of 2048 bytes

// Buffer in RAM for 2048 bytes flash page
static uint8_t page_buffer[PAGE_SIZE];


extern CRC_HandleTypeDef hcrc;

/*
 * Read an amount of bytes from any address
 */
void image_ext_flash_read(uint32_t addr, uint32_t len, uint8_t* data)
{
	uint32_t read_index = 0;
	uint32_t bytes_to_read;

	while( read_index < len )
	{
		// Load page
		w25n01gv_page_data_read( (addr+read_index)/PAGE_SIZE );
		while( w25n01gv_is_busy() )
			HAL_Delay(10);

		bytes_to_read = PAGE_SIZE - ((addr+read_index)%PAGE_SIZE);
		if( (read_index + bytes_to_read) >= len )
			bytes_to_read = len - read_index;

		w25n01gv_fast_read_quad( (addr+read_index)%PAGE_SIZE, bytes_to_read, &data[read_index]);
		read_index += bytes_to_read;
	}
}


/*
 * Check if the image present in a given address is valid
 */
bool image_ext_flash_CRC_is_valid( uint32_t addr )
{
	metadata_fields_v0_t metadata_fields;

	uint32_t read_index = 0;
	uint32_t bytes_to_read;

	uint32_t crc_from_tail;
	uint32_t calculated_crc;

	// Read metadata
	image_ext_flash_read( addr + FW_METADATA_ADDR, sizeof(metadata_fields), (uint8_t*)&metadata_fields);

	// Check presence word
	if( metadata_fields.presence_word != FW_METADATA_PRESENCE_WORD )
		return false;

	// Check sum
	uint32_t sum = 0;
	for( int i=0; i<(sizeof(metadata_fields)/sizeof(uint32_t)); i++ )
		sum += ((uint32_t*)(&metadata_fields))[i];
	if( sum != 0 )
		return false;

	// Get CRC32 present at the end of image
	image_ext_flash_read( addr + metadata_fields.image_size, sizeof(crc_from_tail), (uint8_t*)&crc_from_tail);

	calculated_crc = ~HAL_CRC_Calculate(&hcrc, (uint32_t*)page_buffer, 0); // Reset CRC32 calculator

	// Feed CRC32 calculator
	while( read_index < metadata_fields.image_size )
	{
		bytes_to_read = metadata_fields.image_size - read_index;
		if( bytes_to_read > PAGE_SIZE )
			bytes_to_read = PAGE_SIZE;

		image_ext_flash_read( addr + read_index, bytes_to_read, page_buffer);
		calculated_crc = ~HAL_CRC_Accumulate(&hcrc, (uint32_t*)page_buffer, bytes_to_read);
		read_index += bytes_to_read;
	}

	if( calculated_crc == crc_from_tail )
		return true;
	else
		return false;
}


