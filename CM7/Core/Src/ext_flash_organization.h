/*
 * 
 * External Flash Memory Organization
 * 
 * 
 * Author: André Cascadan, Carlos R. Dell'Aquila
 * 
 */


#ifndef EXTERNAL_FLASH_ORGANIZATION_H
#define EXTERNAL_FLASH_ORGANIZATION_H

/*
 *  - openIPMC-HW has a W25N01GV 1G-bit external flash memory.
 *
 *  - This memory array is organized into 65,53 pages of 2048 bytes (2KB)
 *    and it is 128MB in totally.
 *
 *  - Pages can be erase in groups of 64 (128KB block erase).
 *
 *  - Memory is 1024 blocks.
 *
 */


/*
 * Firmware Backup Area.
 *
 * During the FW upgrade, this region is used to store a copy of the current
 * firmware components (Bootloader, CM7 and CM4) in the External Flash Memory.
 *
 */

/*
 * CM7 Backup Area.
 *
 * 1 MB = 8 blocks of 128KB each one.
 */
#define EXT_FLASHORG_BACKUP_CM7_AREA_START_BLOCK    0
#define EXT_FLASHORG_BACKUP_CM7_AREA_NUM_OF_BLOCKS  8

/*
 * CM4 Backup Area.
 * 
 * 256 KB = 2 blocks of 128KB each one.
 */
#define EXT_FLASHORG_BACKUP_CM4_AREA_START_BLOCK    8
#define EXT_FLASHORG_BACKUP_CM4_AREA_NUM_OF_BLOCKS  2

/*
 * Bootloader (BL7) Backup Area.
 * 
 * 128KB = 1 block of 128KB
 */
#define EXT_FLASHORG_BACKUP_BL7_AREA_START_BLOCK    10
#define EXT_FLASHORG_BACKUP_BL7_AREA_NUM_OF_BLOCKS  1

/*
 * Temporary Area. 
 * 
 * During the FW upgrade, this region is used to store the new uploaded
 * firmware component in the External Flash Memory.
 *
 */

/*
 * CM7 Temp Area.
 *
 * 1 MB = 8 blocks of 128KB each one.
 */
#define EXT_FLASHORG_CM7_TEMP_AREA_START_BLOCK    11
#define EXT_FLASHORG_CM7_TEMP_AREA_NUM_OF_BLOCKS  8

/*
 * CM4 Temp Area.
 *
 * 256 KB = 2 blocks of 128KB each one.
 */
#define EXT_FLASHORG_CM4_TEMP_AREA_START_BLOCK    19
#define EXT_FLASHORG_CM4_TEMP_AREA_NUM_OF_BLOCKS  2

/*
 * Bootloader (BL7) Temp Area.
 * 
 * 128KB = 1 block of 128KB
 */
#define EXT_FLASHORG_BL7_TEMP_AREA_START_BLOCK    21
#define EXT_FLASHORG_BL7_TEMP_AREA_NUM_OF_BLOCKS  1


/*
 * YAFFS2 File System Area in the External Flash Memory
 * 
 * 1968 Kbytes
 */
#define EXT_FLASHORG_YAFFS_START_BLOCK    22
#define EXT_FLASHORG_YAFFS_NUM_OF_BLOCKS  984


/*
 * NOTES:
 * 
 * - The last 24 blocks are not guaranteed to be usable due to eventual
 *   Bad Block Management Look-Up-Table (BBM LUT) remappin.
 * 
 * - 1 Block = 2048 bytes
 */


#endif /* EXTERNAL_FLASH_ORGANIZATION_H */

