/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

#ifndef IMAGE_EXT_FLASH_H
#define IMAGE_EXT_FLASH_H

#include <stdint.h>
#include <stdbool.h>

// Flash characteristics
#define EXT_FLASH_BLOCK_SIZE 131072 // Size of erasable blcok: 128KB or 64 pages = 131072 bytes
#define EXT_FLASH_PAGE_SIZE    2048 // Flash is programmed in pages of 2048 bytes

void image_ext_flash_read(uint32_t addr, uint32_t len, uint8_t* data);
bool image_ext_flash_CRC_is_valid( uint32_t addr );

#endif
