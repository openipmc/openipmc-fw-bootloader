
#include <stdbool.h>
#include <string.h>

#include "main.h"

#include "bootloader_ctrl.h"
#include "fw_metadata.h"
#include "image_ext_flash.h"
#include "image_int_flash.h"
#include "ext_flash_organization.h"

// Buffer to swap data from external flash to internal flash
#define UPGRADE_BUFFER_SIZE 16384
uint8_t upgrade_buffer[UPGRADE_BUFFER_SIZE];


__attribute__ ((section (".bootloader_cmd"))) boot_ctrl_v0_t boot_ctrl_field;

static void update_ctrl_data_load_none( void );
static bool ctrl_data_is_valid( void );
static bool copy_from_ext_flash_to_run( uint32_t ext_origin_addr, uint32_t int_dest_addr );
static void launch_firmware(void);

// Internal Flash operations
void image_int_flash_open( int sector_number );
int image_int_flash_write( uint8_t* data, int len );
int image_int_flash_close( void );
bool image_int_flash_CRC_is_valid( int sector_number, uint32_t expected_crc32 );

// Handlers used here for de-init
extern CRC_HandleTypeDef  hcrc;
extern QSPI_HandleTypeDef hqspi;
extern UART_HandleTypeDef huart4;


/* Firmware component flash memory areas */
extern const uint32_t openipmc_bl7_run_addr;
extern const uint32_t openipmc_cm7_run_addr;
extern const uint32_t openipmc_cm4_run_addr;


/*
 * Bootloader main flow
 */
void bootloader_main_flow(void)
{
  __HAL_RCC_BKPRAM_CLK_ENABLE();

  char* presentation = "\r\nOpenIPMC-FW Bootloader \r\n\r\n";
  HAL_UART_Transmit(&huart4, (uint8_t*)presentation, strlen(presentation), 1000);

  if( ctrl_data_is_valid() )
  {
    // Cortex-M4 Firmware Upgrade
    if( boot_ctrl_field.cm4_load_mode == BOOT_CTRL_LOAD_ADDR_FROM_EXT_FLASH )
    {
      copy_from_ext_flash_to_run(EXT_FLASHORG_CM4_TEMP_AREA_START_BLOCK*EXT_FLASH_BLOCK_SIZE,(uint32_t) &openipmc_cm4_run_addr);
    }
    else if( boot_ctrl_field.cm4_load_mode == BOOT_CTRL_LOAD_BACKUP_FROM_EXT_FLASH )
    {
      copy_from_ext_flash_to_run(EXT_FLASHORG_BACKUP_CM4_AREA_START_BLOCK*EXT_FLASH_BLOCK_SIZE,(uint32_t) &openipmc_cm4_run_addr);
    }

    // Cortex-M7 Firmware Upgrade which is the main component     
    if( boot_ctrl_field.cm7_load_mode == BOOT_CTRL_LOAD_ADDR_FROM_EXT_FLASH )
    {
      copy_from_ext_flash_to_run( EXT_FLASHORG_CM7_TEMP_AREA_START_BLOCK*EXT_FLASH_BLOCK_SIZE,(uint32_t) &openipmc_cm7_run_addr);      
    }
    else if( boot_ctrl_field.cm7_load_mode == BOOT_CTRL_LOAD_BACKUP_FROM_EXT_FLASH )
    {
      copy_from_ext_flash_to_run(EXT_FLASHORG_BACKUP_CM7_AREA_START_BLOCK*EXT_FLASH_BLOCK_SIZE,(uint32_t) &openipmc_cm7_run_addr);     
    }
        
  }
 
  presentation = "\r\nUpdate control data none\r\n\r\n";
  HAL_UART_Transmit(&huart4, (uint8_t*)presentation, strlen(presentation), 1000);

  
  //Guarantee Load Mode as NONE
  update_ctrl_data_load_none();

  // Firmware is directly launched because the control data is INVALID or is NOT PRESENT
  launch_firmware();  
  
}


static bool ctrl_data_is_valid( void )
{
  //Check presence word
  if( boot_ctrl_field.presence_word != BOOT_CTRL_PRESENCE_WORD )
    return false;

  // Check sum
  uint32_t sum = 0;
  for( int i = 0; i < sizeof(boot_ctrl_field)/4; i++)
    sum += ((uint32_t*)&boot_ctrl_field)[i];

  if( sum != 0 )
    return false;

  return true;
}


static void update_ctrl_data_load_none( void )
{
   // Unlock
  PWR->CR1 |= PWR_CR1_DBP;
  while((PWR->CR1 & PWR_CR1_DBP) == 0)
    asm("NOP");

  boot_ctrl_field.cm7_load_mode = BOOT_CTRL_LOAD_NONE;
  
  while(boot_ctrl_field.cm7_load_mode != BOOT_CTRL_LOAD_NONE)
    asm("NOP");
  
  boot_ctrl_field.cm4_load_mode = BOOT_CTRL_LOAD_NONE;

  while(boot_ctrl_field.cm4_load_mode != BOOT_CTRL_LOAD_NONE)
    asm("NOP");
 
  // Check sum
  uint32_t sum = 0;
  for( int i = 0; i < (sizeof(boot_ctrl_field)/4) - 1; i++)
    sum += ((uint32_t*)&boot_ctrl_field)[i];

  boot_ctrl_field.checksum = (~sum)+1;

  //char*  presentation = "\r\n.\n\r";
  //HAL_UART_Transmit(&huart4, (uint8_t*)presentation, strlen(presentation), 1000);
  //asm("NOP");
  
  SCB_CleanDCache_by_Addr((uint32_t*)&boot_ctrl_field, sizeof(&boot_ctrl_field));

  //Verification before Lock (to avoid Lock before data write is complete)
  while( boot_ctrl_field.checksum != (~sum)+1 )
    asm("NOP");

  //Lock
  PWR->CR1 &= ~PWR_CR1_DBP;  
}

/*
 * Upgrade operation.
 *
 * This function copies the content from "addr" in external flat into
 * the RUN area in internal flash.
 *
 * Before copy, it checks the integrity of the content in "addr".
 */
static bool copy_from_ext_flash_to_run( uint32_t ext_origin_addr, uint32_t int_dest_addr )
{
  metadata_fields_v0_t metadata_fields;
  int int_dest_sector = int_dest_addr / INT_FLASH_SECTOR_SIZE;

  uint32_t read_index = 0;
  uint32_t bytes_to_read;

  // Check integrity, otherwise don't copy
  if( !image_ext_flash_CRC_is_valid( ext_origin_addr ) )
    return false;

  // Read metadata
  image_ext_flash_read( ext_origin_addr+FW_METADATA_ADDR, sizeof(metadata_fields), (uint8_t*)&metadata_fields);

  char* message1 = "Copying image from ext flash (TEMP: block x ) to internal flash\r\n";
  HAL_UART_Transmit(&huart4, (uint8_t*)message1, strlen(message1), 1000);

  // Write internal flash
  image_int_flash_open( int_dest_sector );

  while( read_index < metadata_fields.image_size )
  {
    bytes_to_read = metadata_fields.image_size - read_index;
    
    if( bytes_to_read > UPGRADE_BUFFER_SIZE )
      bytes_to_read = UPGRADE_BUFFER_SIZE;

    image_ext_flash_read( ext_origin_addr + read_index, bytes_to_read, upgrade_buffer );
    image_int_flash_write( upgrade_buffer, bytes_to_read );
    read_index += bytes_to_read;

    HAL_UART_Transmit(&huart4, (uint8_t*)".", 1, 1000); // progress indicator
  }
  
  image_int_flash_close();

  char* message2 = "\r\nCopy complete\r\n";
  HAL_UART_Transmit(&huart4, (uint8_t*)message2, strlen(message2), 1000);

  return true;
}


/*
 * This function executes a jump into the target firmware.
 *
 * It must take care of all the de-initializations before jump
 */
static void launch_firmware(void)
{
  //uint32_t i=0;
  void (*jump_to_firmware)(void);


  // De-initializations
  __disable_irq();   // Disable all interrupts
  SysTick->CTRL = 0; // Disable Systick timer
  HAL_RCC_DeInit();  // Set the clock to the default state
  HAL_CRC_DeInit( &hcrc );
  HAL_QSPI_DeInit( &hqspi );
  HAL_UART_DeInit(&huart4);
  HAL_DeInit();

  for (int i=0;i<5;i++)  // Clear Interrupt Enable Register & Interrupt Pending Register
  {
    NVIC->ICER[i]=0xFFFFFFFF;
	NVIC->ICPR[i]=0xFFFFFFFF;
  }


  // Set the address of the entry point to the firmware
  volatile uint32_t BootAddr = (uint32_t) &openipmc_cm7_run_addr;

  // Set up the jump address to firmware being launched (FIRMWARE_ENTRY_POINT + 4)
  jump_to_firmware = (void (*)(void)) (*((uint32_t *) ((BootAddr + 4))));

  //Set the main stack pointer to the firmware being launched
  __set_MSP(*(uint32_t *)BootAddr);


  // Unblock CM4 to boot normally
  SYSCFG->UR1 |= SYSCFG_UR1_BCM4;
  RCC->GCR    |= RCC_GCR_BOOT_C1;

  //Call the function to jump to firmware being launched
  jump_to_firmware();

  // Jump is done successfully
  while (1)
  {
    // Code should never reach this loop
  }
}

