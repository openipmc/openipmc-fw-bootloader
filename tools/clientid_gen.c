
/*
 * ClientID hexa string generator
 * 
 * This program generates the ClientID hexadecimal string in command line given an 
 * ATCA Shelf Address string and an ATCA physical slot number.
 * 
 * This string is useful when configuring a DHCP server to lease IP address to IPMCs
 * according to CERN specifications.
 * 
 * Compile:
 *     gcc clientid_gen.c -o clientid_gen
 * 
 * Use:
 * 
 *     Example:
 *         ATCA Shelf Address: ATCA-40-SB/01/R01-16
 *         ATCA Slot Number: 5
 *     
 *     Run:
 *         $ ./clientid_gen "ATCA-40-SB/01/R01-16" 5
 * 
 *     Output:
 *         ClientID: FF:00:00:00:02:00:02:00:00:31:5A:48:50:4D:2E:33:2D:31:D4:41:54:43:41:2D:34:30:2D:53:42:2F:30:31:2F:52:30:31:2D:31:36:00:00:05:00:00
 * 
 *       
 * Author: André Cascadan
 * 
 */



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>



// This definition follows CERN EDMS 2735323 (rev. 1.0 on 09/06/2022)
// "Custom hardware network interface specification for Phase-2 CMS"
// "Applicable to ATCA on-board controllers and any other custom network devices"
#define CLIENTID_SIZE                  (46U)
#define CLIENTID_HEADER_Pos             (0U)
#define CLIENTID_HEADER_Len             (1U)
#define CLIENTID_DATALENGTH_Pos         (1U)
#define CLIENTID_DATALENGTH_Len         (1U)
#define CLIENTID_TYPE_Pos               (2U)
#define CLIENTID_TYPE_Len               (1U)
#define CLIENTID_IAID_Pos               (3U)
#define CLIENTID_IAID_Len               (4U)
#define CLIENTID_DUIDFMT_Pos            (7U)
#define CLIENTID_DUIDFMT_Len            (2U)
#define CLIENTID_DUIDHDR1_Pos           (9U)
#define CLIENTID_DUIDHDR1_Len           (4U)
#define CLIENTID_DUIDHDR2_Pos          (13U)
#define CLIENTID_DUIDHDR2_Len           (5U)
#define CLIENTID_DUIDHDR3_Pos          (18U)
#define CLIENTID_DUIDHDR3_Len           (2U)
#define CLIENTID_DUIDSHADTYPE_Pos      (20U)
#define CLIENTID_DUIDSHADTYPE_Len       (1U)
#define CLIENTID_DUIDSHAD_Pos          (21U)
#define CLIENTID_DUIDSHAD_Len          (20U)
#define CLIENTID_DUIDSHTYPE_Pos        (41U)
#define CLIENTID_DUIDSHTYPE_Len         (1U)
#define CLIENTID_DUIDPRISITETYPE_Pos   (42U)
#define CLIENTID_DUIDPRISITETYPE_Len    (1U)
#define CLIENTID_DUIDPRISITENUM_Pos    (43U)
#define CLIENTID_DUIDPRISITENUM_Len     (1U)
#define CLIENTID_DUIDSECSITETYPE_Pos   (44U)
#define CLIENTID_DUIDSECSITETYPE_Len    (1U)
#define CLIENTID_DUIDSECSITENUM_Pos    (45U)
#define CLIENTID_DUIDSECSITENUM_Len     (1U)


#define HPM3_DATA_LENGTH            0x2C
#define HPM3_TYPE                   0xFF
#define HPM3_IAID                   0x00000002
#define HPM3_IAID_0                 0x00
#define HPM3_IAID_1                 0x00
#define HPM3_IAID_2                 0x00
#define HPM3_IAID_3                 0x02
#define HPM3_DUID_FORMAT            0x0002
#define HPM3_DUID_FORMAT_0          0x00
#define HPM3_DUID_FORMAT_1          0x02
#define HPM3_DUID_HEADER1           0x0000315A
#define HPM3_DUID_HEADER1_0         0x00
#define HPM3_DUID_HEADER1_1         0x00
#define HPM3_DUID_HEADER1_2         0x31
#define HPM3_DUID_HEADER1_3         0x5A
#define HPM3_DUID_HEADER2          "HPM.3"
#define HPM3_DUID_HEADER2_0        'H'
#define HPM3_DUID_HEADER2_1        'P'
#define HPM3_DUID_HEADER2_2        'M'
#define HPM3_DUID_HEADER2_3        '.'
#define HPM3_DUID_HEADER2_4        '3'
#define HPM3_DUID_HEADER3          0x2D31
#define HPM3_DUID_HEADER3_0        0x2D
#define HPM3_DUID_HEADER3_1        0x31


#define CMSHPM3_DUIDSHADTYPE        0xD4
#define CMSHPM3_DUIDSHTYPE          0x00 // 0x00 = This component lies inside an ATCA shelf
#define CMSHPM3_DUIDPRISITETYPE     0x00 // 0x00 = This is the IPMC of an ATCA carrier
#define CMSHPM3_DUIDSECSITETYPE     0x00 // 0x00 = The IPMC is a primary site, so secondary is zero
#define CMSHPM3_DUIDSECSITENUM      0x00 // 0x00 = The IPMC is a primary site, so secondary is zero

int main( int argc, char **argv )
{
    
    // Check arguments
    if(argc < 3)
    {
        printf("ERROR: Too few arguments\r\n");
        return 1;
    }
    if( strlen(argv[1]) > 20 )
    {
        printf("ERROR: Shelf Address longer than 20 characters.\r\n");
        return 1;
    }
    
    
    uint8_t client_id[HPM3_DATA_LENGTH];
    
	// Constant part
	client_id[ 0] = HPM3_TYPE;
	client_id[ 1] = HPM3_IAID_0;
	client_id[ 2] = HPM3_IAID_1;
	client_id[ 3] = HPM3_IAID_2;
	client_id[ 4] = HPM3_IAID_3;
	client_id[ 5] = HPM3_DUID_FORMAT_0;
	client_id[ 6] = HPM3_DUID_FORMAT_1;
	client_id[ 7] = HPM3_DUID_HEADER1_0;
	client_id[ 8] = HPM3_DUID_HEADER1_1;
	client_id[ 9] = HPM3_DUID_HEADER1_2;
	client_id[10] = HPM3_DUID_HEADER1_3;
	client_id[11] = HPM3_DUID_HEADER2_0;
	client_id[12] = HPM3_DUID_HEADER2_1;
	client_id[13] = HPM3_DUID_HEADER2_2;
	client_id[14] = HPM3_DUID_HEADER2_3;
	client_id[15] = HPM3_DUID_HEADER2_4;
	client_id[16] = HPM3_DUID_HEADER3_0;
	client_id[17] = HPM3_DUID_HEADER3_1;
	client_id[18] = CMSHPM3_DUIDSHADTYPE;
	
	// From 19 to 38:  CERN defined Shelf Address. See bellow.

	client_id[39] = CMSHPM3_DUIDSHTYPE;
	client_id[40] = CMSHPM3_DUIDPRISITETYPE;
	
	client_id[41] = atoi(argv[2]); // Slot Number

	client_id[42] = CMSHPM3_DUIDSECSITETYPE;
	client_id[43] = CMSHPM3_DUIDSECSITENUM;
    
    
    
    
    
	/* Shelf Address format as specified by CMS:
	* ATCA-{building}-{rack id}-{rack unit-number}
	* e.g. ATCA-40-SB/01/R01-16
	*/
	char* cms_shelf_address = argv[1];
	
	memset( &client_id[19], 0x00, 20 );
	int copy_size = strlen( cms_shelf_address );
	if( copy_size > 20 ) copy_size = 20;
	memcpy( &client_id[19], cms_shelf_address, copy_size );
	
	
	
    // Print output:
    printf("ClientID: ");
	
	for( int i=0; i<HPM3_DATA_LENGTH; i++)
	{
		printf("%.2X", client_id[i]);
		if(i<HPM3_DATA_LENGTH-1)
			printf(":");
	}
		
	printf("\r\n");
    
    return 0;
}
