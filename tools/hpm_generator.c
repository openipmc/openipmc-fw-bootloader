 
#include "hpm_generator.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <openssl/md5.h>
#include <zlib.h>
/*
 * 
 * Tools for generating HPM upgrade file
 * 
 * Author: André Cascadan
 * 
 */



#include <stdint.h>

#include "fw_metadata.h"




void add_image_header(FILE* file, image_header_t header)
{
	
	const int fixed_size = 34;      // Header has 34 fixed bytes + OEM + checksum
	uint8_t image_header_array[34];
	
	// Fill the Image Header
	memcpy( &(image_header_array[0]), "PICMGFWU", 8 );  // Signature
	image_header_array[ 8] = 0x00;                      // Format Version
	image_header_array[ 9] = 0x00;                      // Device ID for ATCA IPMC is always 0
	memcpy( &(image_header_array[10]), &header.manufacturer_id, 3 );	
	memcpy( &(image_header_array[13]), &header.product_id, 2 );
	memcpy( &(image_header_array[15]), &header.time, 4 );
	image_header_array[19] = header.image_capabilities;
	image_header_array[20] = header.components;
	image_header_array[21] = header.self_test_timeout;
	image_header_array[22] = header.rollback_timeout;
	image_header_array[23] = header.inaccessibility_timeot;
	image_header_array[24] = header.earliest_compatible_major_revision;
	image_header_array[25] = header.earliest_compatible_minor_revision;
	image_header_array[26] = header.firmware_revision.major;
	image_header_array[27] = header.firmware_revision.minor;
	image_header_array[28] = header.firmware_revision.aux[0];
	image_header_array[29] = header.firmware_revision.aux[1];
	image_header_array[30] = header.firmware_revision.aux[2];
	image_header_array[31] = header.firmware_revision.aux[3];
	image_header_array[32] = 0; //No OEM data
	image_header_array[33] = 0;
	
	fwrite( image_header_array, 1, fixed_size, file );
	
	// Calculate checksum 
	uint8_t header_sum = 0;
	for( int i=0; i<fixed_size; i++)
		header_sum += image_header_array[i];
	uint8_t checksum = (~header_sum)+1; // Checksum
	
	fwrite( &checksum, 1, 1, file );
	
	
}

void add_backup_componets_action(FILE* file, uint8_t component_mask)
{
	uint8_t action[3];
	action[0] = 0x00;  // Action type: BACKUP
	action[1] = component_mask;
	action[2] = (~(action[0] + action[1]))+1;
	
	fwrite( action, 1, 3, file );
}


void add_prepare_componets_action(FILE* file, uint8_t component_mask)
{
	uint8_t action[3];
	action[0] = 0x01; // Action type: PREPARE
	action[1] = component_mask;
	action[2] = (~(action[0] + action[1]))+1;
	
	fwrite( action, 1, 3, file );
}


void add_upload_firmware_image_action(FILE* file, uint8_t component_number, char* description_string, uint8_t* image_data, uint32_t image_size)
{
	
	metadata_fields_v0_t* metadata = (metadata_fields_v0_t*)&image_data[FW_METADATA_ADDR];
	
	uint8_t action[34];
	action[0] = 0x02;  // Action type: UPLOAD IMAGE
	action[1] = 0x01 << component_number;
	action[2] = (~(action[0] + action[1]))+1;
	
	action[3] = metadata->firmware_revision_major;
	action[4] = metadata->firmware_revision_minor;
	action[5] = metadata->firmware_revision_aux[0];
	action[6] = metadata->firmware_revision_aux[1];
	action[7] = metadata->firmware_revision_aux[2];
	action[8] = metadata->firmware_revision_aux[3];
	
	// Copy string limited in 20 chars + end-of-string
	for(int i=0; i<21; i++) action[9+i] = 0;
	for(int i=0; i<20; i++)
	{
		if(description_string[i] != 0)
			action[9+i] = description_string[i];
		else
			break;
	}
	
	*(uint32_t*)(&action[30]) = image_size + 4; // Include 4 bytes for crc32
	
	// Fill with header
	fwrite( action, 1, 34, file );
	
	// Fill with image data
	fwrite( image_data, 1, image_size, file );
	
	// Calculate CRC32 and add to the beginning of the image
	uLong long_crc = crc32(0L, Z_NULL, 0);
	long_crc = crc32(long_crc, image_data, image_size);
	uint32_t crc = long_crc & 0xFFFFFFFF;
	fwrite( &crc, 4, 1, file );
	
	printf("crc32: %x\n", crc);
	
}

uint8_t* load_binary_from_file(char* file_name, uint32_t* size)
{
	FILE* file = fopen( file_name,"rb" );
	if( file == NULL ) return NULL;
	
	// Get size
	fseek( file, 0L, SEEK_END );
	*size = ftell( file );
	rewind( file );
	
	uint8_t* data = malloc( *size );
	fread( data, 1, *size, file );
	
	fclose( file );
	
	return data;
}


void append_md5(char* file_name)
{
	// Calculates md5 sum
	MD5_CTX ctx;
	uint8_t data;
	uint8_t md5_sum[16];
	
	MD5_Init( &ctx );
	FILE* file = fopen( file_name,"rb" );
	while ( fread( &data, 1, 1, file ) == 1 )
		MD5_Update( &ctx, &data, 1 );
	
	fclose(file);
	MD5_Final( md5_sum, &ctx );
	
	// Append md5 to the file 
	file = fopen( file_name,"ab+" );
	fwrite(md5_sum, 1, 16, file);
	fclose(file);
}
