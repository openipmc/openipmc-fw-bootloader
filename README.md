# Bootloader for OpenIPMC-FW

Perform the firmware upgrade for OpenIPMC-FW.


## General concept

OpenIPMC-FW is designed to be compiled and linked to run at the beginning of STM32 internal Flash (0x800 0000, the default STM32H745 boot address for CM7 core). An upgrade process needs to copy the new firmware in this same place (called here as RUN area).

This bootloader basically copies the new firmware from a temporary area (TEMP area) to the the RUN area. The TEMP need to be previously loaded by the OpenIPMC-FW, and it is located in an arbitrary address of the 128MB External flash present on DIMM.

Bootloader is also capable to copy the content of BACKUP area to the RUN area in case of failure in the current running version, or if the user triggers it manually.

In order to guarantee as much memory as possible for OpenIPMC-FW, this bootloader resides at the last (16th) Flash sector, which starts at the address 0x81E 0000.

<img src="./images/bootloader_in_flash.png" alt="bootloader_in_flash" width="500"/>


## Programming the Binary

If the bootloader .bin file is available, it must be programmed into the STM32 internal Flash, on address **0x081E 0000**. The easiest way to do so is by using the *STM32CubeProgrammer* with DFU protocol (via USB connector present o DIMM), or with STLink cable.

The .bin file can be downloaded from the artifacts in the project repository.
